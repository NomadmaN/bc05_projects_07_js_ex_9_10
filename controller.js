// function take info from form
function layThongTinTuForm() {
  var taiKhoan = document.getElementById("tknv").value;
  var hoTen = document.getElementById("name").value;
  var email = document.getElementById("email").value;
  var matKhau = document.getElementById("password").value;
  var ngayLam = document.getElementById("datepicker").value;
  var luongCoBan = Number(document.getElementById("luongCB").value);
  var chucVu = document.getElementById("chucvu").value;
  var gioLam = Number(document.getElementById("gioLam").value);

  console.log({
    taiKhoan,
    hoTen,
    email,
    matKhau,
    ngayLam,
    luongCoBan,
    chucVu,
    gioLam,
  });

  // create NEW employee object (MUST PUT NEW BEFORE OBJECT)
  var newNV = new NhanVien(
    taiKhoan,
    hoTen,
    email,
    matKhau,
    ngayLam,
    luongCoBan,
    chucVu,
    gioLam
  );
  return newNV;
}

// function show info need be edited to form
function showThongTinLenForm(data) {
  document.getElementById("tknv").value = data.taikhoan;
  document.getElementById("name").value = data.hoten;
  document.getElementById("email").value = data.email;
  document.getElementById("password").value = data.matkhau;
  document.getElementById("datepicker").value = data.ngaylam;
  document.getElementById("luongCB").value = data.luongcoban;
  document.getElementById("chucvu").value = data.chucvu;
  document.getElementById("gioLam").value = data.giolam;
}

// function render
function renderDanhSachNhanVien(array) {
  var contentHTML = "";
  for (var i = 0; i < array.length; i++) {
    var currentNV = array[i];
    // content result in table
    var contentTr = `
        <tr>
            <td>${currentNV.taikhoan}</td>
            <td>${currentNV.hoten}</td>
            <td>${currentNV.email}</td>
            <td>${currentNV.ngaylam}</td>
            <td>${currentNV.chucvu}</td>
            <td>${currentNV.TongLuong()}</td>
            <td>${currentNV.XepLoai()}</td>
            <td style="display: flex">
                <button onclick="xoaNhanVien('${
                  currentNV.taikhoan
                }')" title="Xóa nhân viên" class="btn btn-danger" style="font-size: 14px; margin-right: 5px"><i class="fa fa-times"></i></button>
                <button onclick="suaNhanVien('${
                  currentNV.taikhoan
                }')" title="Cập nhật nhân viên" class="btn btn-warning" style="font-size: 14px;" data-toggle="modal"
                data-target="#myModal"><i class="fa fa-code"></i></button>
            </td>
            
        </tr>
        `;
    contentHTML += contentTr;
  }
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

// function find index
function viTriNhanVien(id, array) {
  for (var i = 0; i < array.length; i++) {
    var viTri = array[i];
    if (viTri.taikhoan == id) {
      return i;
    }
  }
  return -1;
}
// function tìm theo từ khóa
function parseSearchValue(str) {
  if (str == null || str == undefined) return str;
  str = str.toLowerCase();
  str = str.replace(/xuat sac|xuất sắc|xuatsac|xuat|sac|xs/gui, "Xuất sắc");
  str = str.replace(/gioi|giỏi|g/gui, "Giỏi");
  str = str.replace(/kha|khá|kh|k/gui, "Khá");
  str = str.replace(/trung binh|trung bình|trungbinh|trung|binh|tb/gui, "Trung bình");
  return str;
}

// function show error message
function showMessageError(idError, message) {
  document.getElementById(idError).innerHTML = message;
}

// tạo hàm tìm NV khi nhấn Enter
document
  .querySelector("#searchName")
  .addEventListener("keypress", function (input) {
    // nếu nhấn nút Enter
    if (input.key == "Enter") {
      // thì hệ thống tự nhấn nút Search (theo ID)
      document.getElementById("btnTimNV").click();
    }
  });
