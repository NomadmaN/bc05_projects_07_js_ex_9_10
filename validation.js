// validation
// function check input length
function checkInputLength(userInput,minlength,maxlength,resultID){
    var minLen = minlength;
    var maxLen = maxlength;
    if (userInput.length >= minLen && userInput.length <= maxLen){
        document.getElementById(resultID).innerHTML = '';
        return true;
    }else{
        document.getElementById(resultID).innerHTML = 'Vui lòng nhập trong khoảng ' + minLen + ' tới ' + maxLen + ' số!';
        return false;
    }
}
// function check input value
function checkInputValue(userInput,minvalue,maxvalue,resultID){
    var minVal = minvalue;
    var maxVal = maxvalue;
    if (userInput >= minVal && userInput <= maxVal){
        document.getElementById(resultID).innerHTML = '';
        return true;
    }else{
        document.getElementById(resultID).innerHTML = 'Giá trị nhập vào trong khoảng ' + minVal.toLocaleString() + ' tới ' + maxVal.toLocaleString() + '!';
        return false;
    }
}
// function check input = text with space
function checkInputText(userInput,resultID){
    var letterFormat = /^[A-Za-z\s]+$/;
    if (userInput.match(letterFormat)){
        document.getElementById(resultID).innerHTML = '';
        return true;
    }else{
        document.getElementById(resultID).innerHTML = 'Vui lòng chỉ nhập chữ không dấu!';
        return false;
    }
}
// function check input = name/text with unicode
function removeAscent(str) {
    if (str === null || str === undefined) return str;
    str = str.toLowerCase();
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    return str;
}
function isValidUnicode(userInput) {
    var regex = /^[a-zA-Z\s!@#\$%\^\&*\)\(+=._-]{2,}$/g; // regex here
    return regex.test(removeAscent(userInput));
}
function checkInputNameWithUnicode(userInput,resultID){
    if (isValidUnicode(userInput)){
        document.getElementById(resultID).innerHTML = '';
        return true;
    }else{
        document.getElementById(resultID).innerHTML = 'Vui lòng chỉ nhập chữ cho họ tên!';
        return false;
    }
}

// function check input = number
function checkInputNumber(userInput,resultID){
    var numberFormat = /^[0-9]+$/;
    if (userInput.match(numberFormat)){
        document.getElementById(resultID).innerHTML = '';
        return true;
    }else{
        document.getElementById(resultID).innerHTML = 'Vui lòng chỉ nhập số!';
        return false;
    }
}
// function check input = email
function checkInputEmail(userInput,resultID){
    var emailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (userInput.match(emailFormat)){
        document.getElementById(resultID).innerHTML = '';
        return true;
    }else{
        document.getElementById(resultID).innerHTML = 'Nhập sai định dạng email!';
        return false;
    }
}
// function check input = password which 6-10 characters and include 1 uppercase, 1 number & 1 special character
function checkInputPassword(userInput,resultID){
    var passwordFormat = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{6,10}$/;
    if (userInput.match(passwordFormat)){
        document.getElementById(resultID).innerHTML = '';
        return true;
    }else{
        document.getElementById(resultID).innerHTML = 'Mật khẩu 6-10 ký tự & cần ít nhất 1 chữ in Hoa, 1 số, 1 ký tự đặc biệt!';
        return false;
    }
}
// function check input = Date format
function checkInputDate(userInput,resultID){
    let dateFormat = /^(0?[1-9]|1[0-2])[\/](0?[1-9]|[1-2][0-9]|3[01])[\/]\d{4}$/;      
          
    // Match the date format through regular expression      
    if(userInput.match(dateFormat)){      
        let operator = userInput.split('/');      
      
        // Extract the string into month, date and year      
        let datepart = [];      
        if (operator.length>1){      
            datepart = userInput.split('/');      
        }      
        let month= parseInt(datepart[0]);      
        let day = parseInt(datepart[1]);      
        let year = parseInt(datepart[2]);      
              
        // Create list of days of a month      
        let ListofDays = [31,28,31,30,31,30,31,31,30,31,30,31];      
        if (month==1 || month>2){      
            if (day>ListofDays[month-1]){      
                ///This check is for Confirming that the date is not out of its range    
                return false;      
            }      
        }else if (month==2){      
            let leapYear = false;      
            if ( (!(year % 4) && year % 100) || !(year % 400)) {      
                leapYear = true;      
            }      
            if ((leapYear == false) && (day>=29)){  
                document.getElementById(resultID).innerHTML = 'Tháng 2 chỉ có 28 ngày!';     
                return false;      
            }else      
            if ((leapYear==true) && (day>29)){      
                document.getElementById(resultID).innerHTML = 'Tháng 2 năm nhuận chỉ có 29 ngày!';      
                return false;      
            }      
        }      
    }else{      
        document.getElementById(resultID).innerHTML = 'Sai định dạng MM/DD/YYYY!';     
        return false;      
    }      
    return true; 
}
// function check duplication 
function checkDuplication(id,array){
    let index = viTriNhanVien(id,array);
    if (index !== -1){
        showMessageError('tbTKNV','Tài khoản đã tồn tại!');
        return false;
    }else{
        showMessageError('tbTKNV','');
        return true;  
    }
}
// function check null
function checkNull(userInput,idError,message){
    if (userInput.length == 0 || Number(userInput) == 0){
        showMessageError(idError,message);
        return false;
    }else{
        showMessageError(idError,"");
        return true;
    }
}



/* For Learning

1. Text only
/^[A-Za-z]+$/

2. Text with space allowed
/^[A-Za-z\s]+$/

3. Number only
/^[0-9]+$/

4. Email
/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/

5. Password with 6-10 characters, include 1 UPPER case, 1 number & 1 special letter
/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{6,10}$/
// more password format, visit: https://viblo.asia/p/mot-vai-bieu-thuc-chinh-quy-thuong-dung-QpmleoBVKrd

*/