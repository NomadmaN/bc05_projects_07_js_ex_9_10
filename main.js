// key name to save & load storage
const DSNV = 'Danh Sách Nhân Viên';
// define position
const giamDoc = 'Giám đốc';
const truongPhong = 'Trưởng phòng';
const nhanVien = 'Nhân viên';
// define level
const nvXuatSac = 'Xuất sắc';
const nvGioi = 'Giỏi';
const nvKha = 'Khá';
const nvTrungBinh = 'Trung bình';
// main array
var danhSachNhanVien = [];
// class object
function NhanVien(_taikhoan,_hoten,_email,_matkhau,_ngaylam,_luongcoban,_chucvu,_giolam){
    this.taikhoan = _taikhoan;
    this.hoten = _hoten;
    this.email = _email;
    this.matkhau = _matkhau;
    this.ngaylam = _ngaylam;
    this.luongcoban = _luongcoban;
    this.chucvu = _chucvu;
    this.giolam = _giolam;
    this.TongLuong = function(){
        if (this.chucvu == giamDoc){
            return (this.luongcoban * 3).toLocaleString();
        }else if (this.chucvu == truongPhong){
            return (this.luongcoban * 2).toLocaleString();
        }else{
            return this.luongcoban.toLocaleString();
        }
    }
    this.XepLoai = function(){
        if (this.giolam >= 192){
            return nvXuatSac;
        }else if (this.giolam >= 176){
            return nvGioi;
        }else if (this.giolam >= 160){
            return nvKha;
        }else{
            return nvTrungBinh;
        }
    }
}

// Local Storage
// save
function saveLocalStorage() {
    let jsonDanhSachNV = JSON.stringify(danhSachNhanVien);
    console.log('jsonDanhSachNV',jsonDanhSachNV);
    localStorage.setItem(DSNV, jsonDanhSachNV);
}
// load
var dataJson = localStorage.getItem(DSNV);
if (dataJson !== null) {
  var arrayTemp = JSON.parse(dataJson);

  // vì localStorage không lưu function điểm trung bình (dtb) nên phải convert data thông qua array tạm
  for (var i = 0; i < arrayTemp.length; i++) {
    var currentNVTemp = arrayTemp[i];
    var nvSaved = new NhanVien(
      currentNVTemp.taikhoan,
      currentNVTemp.hoten,
      currentNVTemp.email,
      currentNVTemp.matkhau,
      currentNVTemp.ngaylam,
      currentNVTemp.luongcoban,
      currentNVTemp.chucvu,
      currentNVTemp.giolam,
    );
    danhSachNhanVien.push(nvSaved);
    console.log('danhSachNhanVien',danhSachNhanVien);
  }
  renderDanhSachNhanVien(danhSachNhanVien);
}

// CRUD
function themNhanVien(){
    var newNV = layThongTinTuForm();
    console.log('newNV',newNV);

    var isValid = true;
    // check seperate ID
    isValid =
    checkNull(newNV.taikhoan, "tbTKNV", "Mã SV không được để trống") &&
    checkDuplication(newNV.taikhoan, danhSachNhanVien) &&
    checkInputNumber(newNV.taikhoan, 'tbTKNV') &&
    checkInputLength(newNV.taikhoan,4,6,'tbTKNV');
    // check seperate name (always check null first)
    isValid = isValid &
    (checkNull(newNV.hoten, 'tbTen', 'Họ tên không được để trống!') &&
    // checkInputText(newNV.hoten, 'tbTen'));
    checkInputNameWithUnicode(newNV.hoten, 'tbTen'));
    // check seperate email (always check null first)
    isValid = isValid &
    (checkNull(newNV.email, 'tbEmail', 'Email không được để trống!') &&
    checkInputEmail(newNV.email, 'tbEmail'));
    // check seperate password (always check null first)
    isValid = isValid &
    (checkNull(newNV.matkhau, 'tbMatKhau', 'Mật khẩu không được để trống!') &&
    checkInputPassword(newNV.matkhau, 'tbMatKhau'));
    // check seperate day work (always check null first)
    isValid = isValid &
    (checkNull(newNV.ngaylam, 'tbNgay', 'Ngày làm không được để trống!') &&
    checkInputDate(newNV.ngaylam, 'tbNgay'));
    // check seperate salary (always check null first)
    isValid = isValid &
    (checkNull(newNV.luongcoban, 'tbLuongCB', 'Lương không được để trống!') &&
    checkInputValue(newNV.luongcoban,1000000,20000000,'tbLuongCB'));
    // check seperate work hour (always check null first)
    isValid = isValid &
    (checkNull(newNV.giolam, 'tbGiolam', 'Giờ làm không được để trống!') &&
    checkInputValue(newNV.giolam,80,200,'tbGiolam'));
    // last check element with single check only
    isValid = isValid &
    checkNull(newNV.chucvu, 'tbChucVu', 'Chưa chọn chức vụ!');

    if (isValid){
        danhSachNhanVien.push(newNV);
        console.log('danhSachNhanVien',danhSachNhanVien);
        saveLocalStorage();
        renderDanhSachNhanVien(danhSachNhanVien);
    }else{
        // querySelectorAll on selector is an array, make loop to cover all
        const showError = document.querySelectorAll(".modal-body .form-group .sp-thongbao");
        for (let i = 0; i < showError.length; i++) {
        showError[i].style.display = 'block';
        }
    }
}

function xoaNhanVien(id){
    var viTri = viTriNhanVien(id, danhSachNhanVien);
    console.log("Vị trí đã xóa:", viTri);
    if (viTri !== -1){
        danhSachNhanVien.splice(viTri, 1);
        saveLocalStorage();
        renderDanhSachNhanVien(danhSachNhanVien);
    }
}

function suaNhanVien(id){
    var viTri = viTriNhanVien(id, danhSachNhanVien);
    if (viTri == -1) return;
    var nvEdit = danhSachNhanVien[viTri];
    console.log('Data cần edit: ', nvEdit);
    showThongTinLenForm(nvEdit);
    document.getElementById("tknv").disabled = true;
}

function updateNhanVien(){
    var dataEdited = layThongTinTuForm();

    var isValid = true;
    // check seperate name (always check null first)
    isValid = 
    (checkNull(dataEdited.hoten, 'tbTen', 'Họ tên không được để trống!') &&
    // checkInputText(newNV.hoten, 'tbTen'));
    checkInputNameWithUnicode(dataEdited.hoten, 'tbTen'));
    // check seperate email (always check null first)
    isValid = isValid &
    (checkNull(dataEdited.email, 'tbEmail', 'Email không được để trống!') &&
    checkInputEmail(dataEdited.email, 'tbEmail'));
    // check seperate password (always check null first)
    isValid = isValid &
    (checkNull(dataEdited.matkhau, 'tbMatKhau', 'Mật khẩu không được để trống!') &&
    checkInputPassword(dataEdited.matkhau, 'tbMatKhau'));
    // check seperate day work (always check null first)
    isValid = isValid &
    (checkNull(dataEdited.ngaylam, 'tbNgay', 'Ngày làm không được để trống!') &&
    checkInputDate(dataEdited.ngaylam, 'tbNgay'));
    // check seperate salary (always check null first)
    isValid = isValid &
    (checkNull(dataEdited.luongcoban, 'tbLuongCB', 'Lương không được để trống!') &&
    checkInputValue(dataEdited.luongcoban,1000000,20000000,'tbLuongCB'));
    // check seperate work hour (always check null first)
    isValid = isValid &
    (checkNull(dataEdited.giolam, 'tbGiolam', 'Giờ làm không được để trống!') &&
    checkInputValue(dataEdited.giolam,80,200,'tbGiolam'));
    // last check element with single check only
    isValid = isValid &
    checkNull(dataEdited.chucvu, 'tbChucVu', 'Chưa chọn chức vụ!');

    if (isValid){
        var viTri = viTriNhanVien(dataEdited.taikhoan, danhSachNhanVien);
        if (viTri == -1) return;
        danhSachNhanVien[viTri] = dataEdited;
        renderDanhSachNhanVien(danhSachNhanVien);
        saveLocalStorage();
        document.getElementById("tknv").disabled = false;
    }else{
        // querySelectorAll on selector is an array, make loop to cover all
        const showError = document.querySelectorAll(".modal-body .form-group .sp-thongbao");
        for (let i = 0; i < showError.length; i++) {
        showError[i].style.display = 'block';
        }
    }
}

function showID(){
    document.getElementById("tknv").disabled = false;
}

function timKiemNhanVien(){
    // xác định giá trị tìm kiếm
    var thongTinCanTim = document.getElementById('searchName').value;
    // phân tích từ khóa tìm bằng phương thức chuyển hóa từ khóa thành kết quả muốn tìm
    var thongTinParsed = parseSearchValue(thongTinCanTim);
    // chuyển về lowerCase để filter & trả kết quả
    var thongTin = thongTinParsed.toLowerCase();
    console.log('thongTin tìm ra: ', thongTin);
    // dùng hàm filter kết hợp indexOf để lọc thông tin
    var danhSachNhanVienFiltered = danhSachNhanVien.filter(element => element.XepLoai().toLowerCase().indexOf(thongTin) > -1);
    renderDanhSachNhanVien(danhSachNhanVienFiltered);
}

// // cách 2 tìm kiếm
// function timKiemNhanVien(){
//     var thongTinTimKiem = document.getElementById('searchName').value;
//     var danhSachSearched = [];
//     if (thongTinTimKiem == '') return renderDanhSachNhanVien(danhSachNhanVien);

//     var regexXuatSac = /xuat sac|xuất sắc|xuatsac|xs/ui;
//     var regexGioi = /gioi|giỏi|gi/ui;
//     var regexKha = /kha|khá|kh|k/ui;
//     var regexTrungBinh = /trung binh|trung bình|trungbinh|tb/ui;

//     if (regexXuatSac.test(thongTinTimKiem) == true) {
//         danhSachNhanVien.forEach(function (element, index){
//             if (element.XepLoai() == 'Xuất sắc'){
//                 danhSachSearched.push(danhSachNhanVien[index])
//             }
//         });
//     }else if (regexGioi.test(thongTinTimKiem) == true) {
//         danhSachNhanVien.forEach(function (element, index){
//             if (element.XepLoai() == 'Giỏi'){
//                 danhSachSearched.push(danhSachNhanVien[index])
//             }
//         });
//     }else if (regexKha.test(thongTinTimKiem) == true) {
//         danhSachNhanVien.forEach(function (element, index){
//             if (element.XepLoai() == 'Khá'){
//                 danhSachSearched.push(danhSachNhanVien[index])
//             }
//         });
//     }else if (regexTrungBinh.test(thongTinTimKiem) == true) {
//         danhSachNhanVien.forEach(function (element, index){
//             if (element.XepLoai() == 'Trung bình'){
//                 danhSachSearched.push(danhSachNhanVien[index])
//             }
//         });
//     }
//     renderDanhSachNhanVien(danhSachSearched);
// }